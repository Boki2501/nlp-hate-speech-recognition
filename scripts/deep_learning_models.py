from scripts.tcn_model import TCN
from tensorflow.keras.layers import SpatialDropout1D, GlobalAveragePooling1D, GlobalMaxPooling1D
from tensorflow.keras.layers import Conv1D, MaxPool1D, Flatten, Bidirectional, GRU, LSTM, Embedding
from tensorflow.keras.layers import Dense, Dropout, concatenate, Input, BatchNormalization
from tensorflow.keras.models import Sequential
from tensorflow.keras.constraints import MaxNorm

from tensorflow.keras import Model


def tcn_model(bert_model, activation='relu', seq_len=128,
              loss='binary_crossentropy', optimizer='adam', model_summary=False):
    input_ids = Input(shape=(seq_len,), name='input_ids', dtype='int32')
    mask = Input(shape=(seq_len,), name='attention_mask', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings = bert_model(input_ids, attention_mask=mask)[0]

    x = SpatialDropout1D(0.1)(embeddings)

    x = TCN(128, dilations=[1, 2, 4], return_sequences=True, activation='relu', name='tcn1')(x)
    x = TCN(64, dilations=[1, 2, 4], return_sequences=True, activation='relu', name='tcn2')(x)

    avg_pool = GlobalAveragePooling1D()(x)
    max_pool = GlobalMaxPooling1D()(x)

    concatenation = concatenate([avg_pool, max_pool])
    y = Dense(16, activation="relu")(concatenation)
    y = Dropout(0.1)(y)
    output = Dense(1, activation="sigmoid", name='outputs')(y)

    # define input and output layers of our model
    model = Model(inputs=[input_ids, mask], outputs=output)

    # freeze the BERT layer - otherwise we will be training 100M+ parameters...
    model.layers[2].trainable = False

    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

    if model_summary:
        print(model.summary())

    return model


def cnn_biGru_model(bert_model, activation='relu', seq_len=128,
                    filters=100, kernel_size=3, loss='binary_crossentropy',
                    optimizer='adam', model_summary=False):
    # Channel 1D
    input_ids1 = Input(shape=(seq_len,), name='input_ids1', dtype='int32')
    mask1 = Input(shape=(seq_len,), name='attention_mask1', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings1 = bert_model(input_ids1, attention_mask=mask1)[0]

    conv1 = Conv1D(filters=filters, kernel_size=kernel_size, activation='relu',
                   kernel_constraint=MaxNorm(max_value=3, axis=[0, 1]))(embeddings1)
    pool1 = MaxPool1D(pool_size=2, strides=2)(conv1)
    flat1 = Flatten()(pool1)
    drop1 = Dropout(0.5)(flat1)
    dense1 = Dense(10, activation='relu')(drop1)
    drop1 = Dropout(0.5)(dense1)
    out1 = Dense(1, activation='sigmoid')(drop1)

    # Channel BiGRU
    input_ids2 = Input(shape=(seq_len,), name='input_ids2', dtype='int32')
    mask2 = Input(shape=(seq_len,), name='attention_mask2', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings2 = bert_model(input_ids2, attention_mask=mask2)[0]

    gru2 = Bidirectional(GRU(64))(embeddings2)
    drop2 = Dropout(0.5)(gru2)
    out2 = Dense(1, activation='sigmoid')(drop2)

    # Merge
    merged = concatenate([out1, out2])

    # Interpretation
    outputs = Dense(1, activation='sigmoid')(merged)
    model = Model(inputs=[input_ids1, mask1, input_ids2, mask2], outputs=outputs)

    # freeze the BERT layer - otherwise we will be training 100M+ parameters...
    model.layers[2].trainable = False

    # Compile
    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

    if model_summary:
        print(model.summary())

    return model


def cnn_biLSTM_model(bert_model, activation='relu', seq_len=128,
                     filters=100, kernel_size=3, loss='binary_crossentropy',
                     optimizer='adam', model_summary=False):
    # Channel 1D
    input_ids1 = Input(shape=(seq_len,), name='input_ids1', dtype='int32')
    mask1 = Input(shape=(seq_len,), name='attention_mask1', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings1 = bert_model(input_ids1, attention_mask=mask1)[0]

    conv1 = Conv1D(filters=filters, kernel_size=kernel_size, activation='relu',
                   kernel_constraint=MaxNorm(max_value=3, axis=[0, 1]))(embeddings1)
    pool1 = MaxPool1D(pool_size=2, strides=2)(conv1)
    flat1 = Flatten()(pool1)
    drop1 = Dropout(0.5)(flat1)
    dense1 = Dense(10, activation='relu')(drop1)
    drop1 = Dropout(0.5)(dense1)
    out1 = Dense(1, activation='sigmoid')(drop1)

    # Channel BiGRU
    input_ids2 = Input(shape=(seq_len,), name='input_ids2', dtype='int32')
    mask2 = Input(shape=(seq_len,), name='attention_mask2', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings2 = bert_model(input_ids2, attention_mask=mask2)[0]

    lstm2 = Bidirectional(LSTM(64))(embeddings2)
    drop2 = Dropout(0.5)(lstm2)
    out2 = Dense(1, activation='sigmoid')(drop2)

    # Merge
    merged = concatenate([out1, out2])

    # Interpretation
    outputs = Dense(1, activation='sigmoid')(merged)
    model = Model(inputs=[input_ids1, mask1, input_ids2, mask2], outputs=outputs)

    # freeze the BERT layer - otherwise we will be training 100M+ parameters...
    model.layers[2].trainable = False

    # Compile
    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

    if model_summary:
        print(model.summary())

    return model


def lstm_model(bert_model, activation='relu', seq_len=128,
               loss='binary_crossentropy', optimizer='adam', model_summary=False):
    input_ids = Input(shape=(seq_len,), name='input_ids', dtype='int32')
    mask = Input(shape=(seq_len,), name='attention_mask', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings = bert_model(input_ids, attention_mask=mask)[0]

    X = LSTM(64)(embeddings)
    X = BatchNormalization()(X)
    X = Dense(64, activation='relu')(X)
    X = Dropout(0.1)(X)
    y = Dense(1, activation='sigmoid', name='outputs')(X)

    # define input and output layers of our model
    model = Model(inputs=[input_ids, mask], outputs=y)

    # freeze the BERT layer - otherwise we will be training 100M+ parameters...
    model.layers[2].trainable = False

    # Compile
    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

    if model_summary:
        print(model.summary())

    return model


def biLstm_model(bert_model, activation='relu', seq_len=128,
                 loss='binary_crossentropy', optimizer='adam', model_summary=False):
    input_ids = Input(shape=(seq_len,), name='input_ids', dtype='int32')
    mask = Input(shape=(seq_len,), name='attention_mask', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings = bert_model(input_ids, attention_mask=mask)[0]

    X = Bidirectional(LSTM(64))(embeddings)
    X = BatchNormalization()(X)
    X = Dense(64, activation='relu')(X)
    X = Dropout(0.1)(X)
    y = Dense(1, activation='sigmoid', name='outputs')(X)

    # define input and output layers of our model
    model = Model(inputs=[input_ids, mask], outputs=y)

    # freeze the BERT layer - otherwise we will be training 100M+ parameters...
    model.layers[2].trainable = False

    # Compile
    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

    if model_summary:
        print(model.summary())

    return model


def stacked_biLstm_model(bert_model, activation='relu', seq_len=128,
                         loss='binary_crossentropy', optimizer='adam', model_summary=False):
    input_ids = Input(shape=(seq_len,), name='input_ids', dtype='int32')
    mask = Input(shape=(seq_len,), name='attention_mask', dtype='int32')

    # we consume the last_hidden_state tensor from bert (discarding pooled_outputs)
    embeddings = bert_model(input_ids, attention_mask=mask)[0]

    X = Bidirectional(LSTM(64, return_sequences=True))(embeddings)
    X = Bidirectional(LSTM(64, return_sequences=True))(X)
    X = BatchNormalization()(X)
    X = Dense(64, activation='relu')(X)
    X = Dropout(0.1)(X)
    y = Dense(1, activation='sigmoid', name='outputs')(X)

    # define input and output layers of our model
    model = Model(inputs=[input_ids, mask], outputs=y)

    # freeze the BERT layer - otherwise we will be training 100M+ parameters...
    model.layers[2].trainable = False

    # Compile
    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

    if model_summary:
        print(model.summary())

    return model


def sequential_model(seq_len=1000, vocab_size=1000, loss='binary_crossentropy', optimizer='adam', model_summary=False):
    model = Sequential()
    model.add(Dense(64, activation='relu', input_dim=seq_len))
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.1))
    model.add(Dense(1, activation='sigmoid', name='outputs'))

    model.compile(loss=loss, optimizer=optimizer, metrics=['accuracy'])

    return model
