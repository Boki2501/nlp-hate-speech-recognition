from tensorflow.data import Dataset


def map_func_parallel_inputs(input_ids, masks, labels):
    return {'input_ids1': input_ids, 'attention_mask1': masks, 'input_ids2': input_ids,
            'attention_mask2': masks}, labels


def map_func(input_ids, masks, labels):
    return {'input_ids': input_ids, 'attention_mask': masks}, labels


def create_dataset(input_ids, attention_masks, labels, parallel_inputs=False):
    BATCH_SIZE = 64  # we will use batches of 64

    dataset = Dataset.from_tensor_slices((input_ids, attention_masks, labels))
    # using map method to apply map_func to dataset
    if parallel_inputs:
        dataset = dataset.map(map_func_parallel_inputs)
    else:
        dataset = dataset.map(map_func)

    # shuffle data and batch it
    dataset = dataset.shuffle(100).batch(BATCH_SIZE)
    return dataset
