import tensorflow_hub as hub
from transformers import BertTokenizer, TFBertModel, BertConfig
from transformers import TFXLMRobertaModel, XLMRobertaTokenizer, XLMRobertaConfig
from transformers import DistilBertTokenizer, TFDistilBertModel, DistilBertConfig

import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer

universal_sentence_encoder_url = "https://tfhub.dev/google/universal-sentence-encoder-multilingual/3"
mbert_url = "bert-base-multilingual-cased"  # used as tokenizer and model
xlmRoberta_url = "jplu/tf-xlm-roberta-base"  # used as tokenizer and model
distilBert_url = "distilbert-base-multilingual-cased"  # used as tokenizer and model


def load_universal_sentence_encoder():
    """
    Function for loading Universal Sentence Encoder
    :return: model
    """
    model = hub.load(universal_sentence_encoder_url)
    print("module %s loaded" % universal_sentence_encoder_url)
    return model


def use_encode(model, texts: pd.Series):
    """
    :param model: universal sentence encoder model
    :param texts: df column of texts that we want to encode
    :return: list of 512-sized vectors for each sentence
    """
    train_embeddings = texts.apply(lambda x: model([x]))
    train_embeddings = np.array([np.array(x[0]).tolist() for x in train_embeddings])
    return train_embeddings


def tf_idf(train_docs,
           test_docs,
           max_features=3000,
           min_df=5,
           max_df=0.75,
           use_idf=True,
           ngram_range=(2, 7)):
    """
    :param test_docs:
    :param train_docs:
    :param max_features:
    :param min_df:
    :param max_df:
    :param use_idf:
    :param ngram_range:
    :return:
    """
    tv = TfidfVectorizer(max_features=max_features,
                         min_df=min_df,
                         max_df=max_df,
                         use_idf=use_idf, ngram_range=ngram_range)
    tv_docs_train = tv.fit_transform(train_docs)
    tv_docs_test = tv.transform(test_docs)
    tv_docs_train = tv_docs_train.toarray()
    tv_docs_test = tv_docs_test.toarray()

    return tv_docs_train, tv_docs_test


def load_mbert():
    """
    Function that loads and returns mbert tokenizer and model
    :return:
    """
    tokenizer = BertTokenizer.from_pretrained(mbert_url)
    model = TFBertModel.from_pretrained(mbert_url)

    return tokenizer, model


def load_xlmRoberta():
    config = XLMRobertaConfig.from_pretrained(xlmRoberta_url, output_hidden_states=True, output_attentions=True,
                                              use_cache=True)
    tokenizer = XLMRobertaTokenizer.from_pretrained(xlmRoberta_url)
    model = TFXLMRobertaModel.from_pretrained(xlmRoberta_url, config=config)

    return tokenizer, model


def load_distilBert():
    config = DistilBertConfig.from_pretrained(distilBert_url, output_hidden_states=True, output_attentions=True,
                                              use_cache=True)
    tokenizer = DistilBertTokenizer.from_pretrained(distilBert_url)
    model = TFDistilBertModel.from_pretrained(distilBert_url, config=config)

    return tokenizer, model


def tokenize(tokenizer, sentence, SEQ_LEN):
    tokens = tokenizer.encode_plus(sentence, max_length=SEQ_LEN,
                                   truncation=True, padding='max_length',
                                   add_special_tokens=True, return_attention_mask=True,
                                   return_token_type_ids=False, return_tensors='tf')
    return tokens['input_ids'], tokens['attention_mask']


def bert_encode(tokenizer, texts: pd.Series, SEQ_LEN=128):
    """

    :param tokenizer: Any BERT tokenizer
    :param texts: df column of texts that we want to encode
    :param SEQ_LEN: length of the tokenized sequences
    :return:
    """
    # initialize two arrays for input tensors
    input_ids = np.zeros((len(texts), SEQ_LEN))
    attention_masks = np.zeros((len(texts), SEQ_LEN))

    for i, sentence in enumerate(texts):
        input_ids[i, :], attention_masks[i, :] = tokenize(tokenizer, sentence, SEQ_LEN)
    return input_ids, attention_masks
